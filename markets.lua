--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 of 3RaGaming
--This file holds the functions that involve market management

require "locale/utils/event"

cell_processed = cell_processed or script.generate_event_name()
phase_change = phase_change or script.generate_event_name()

--List of offers for a player to buy
--Format: {cost-in-coins, reward}
local buy_offers = {
	{1, {type="give-item", item="iron-plate", count=400}},
	{1, {type="give-item", item="copper-plate", count=400}},
	{1, {type="give-item", item="coal", count=400}},
	{1, {type="give-item", item="electronic-circuit", count=200}},
	{3, {type="give-item", item="piercing-rounds-magazine", count=10}},
	{3, {type="give-item", item="advanced-circuit", count=200}},
	{5, {type="give-item", item="uranium-rounds-magazine", count=3}},
	{5, {type="gun-speed", ammo_category="bullet", modifier=0.01}},
	{10, {type="give-item", item="assembling-machine-3", count=10}},
	{10, {type="give-item", item="space-science-pack", count=500}},
	{10, {type="inserter-stack-size-bonus", modifier=1}},
	{10, {type="worker-robot-speed", modifier=0.05}},
	{12, {type="give-item", item="processing-unit", count=100}},
	{15, {type="num-quick-bars", modifier=1}},
	{20, {type="character-logistic-slots", modifier=6}},
	{25, {type="laboratory-speed", modifier=0.50}}
}

--List of offers for a player to sell
--Format: {worth-in-coins, sell}
local sell_offers = {
	{1, {"iron-plate", 800}},
	{1, {"copper-plate", 800}},
	{1, {"coal", 800}},
	{1, {"science-pack-1", 400}},
	{2, {"science-pack-2", 400}},
	{5, {"logistic-robot", 100}},
	{5, {"construction-robot", 100}},
	{12, {"science-pack-3", 400}}
}

--Reset the offers table if it's empty
local function reset_offers()
	for _,offer in pairs(buy_offers) do
		table.insert(global.offers, {"buy", offer})
	end
	for _,offer in pairs(sell_offers) do
		table.insert(global.offers, {"sell", offer})
	end
end

--Function to add three random offers from the above offer lists to the market
--Market is a LuaEntity that must be "market"
local function randomize_market(market)
	if market.name ~= "market" then return end

	--Remove the old items
	local items = market.get_market_items()
	if items then for i=#items,1,-1 do market.remove_market_item(i) end end

	for _=1,3 do
		if #global.offers == 0 then reset_offers() end

		local offer
		local randomnum = math.random(#global.offers)
		local random_offer = global.offers[randomnum]
		local offer_type = random_offer[1]

		if offer_type == "buy" then
			--Add a buy offer
			offer = {price = {{"coin", random_offer[2][1]}}, offer = random_offer[2][2]}
			--game.print("Buying " .. (random_offer[2][2]["item"] or random_offer[2][2]["type"]) .. " for " .. random_offer[2][1] .. "coins.")
		else
			--Add a seller offer
			offer = {price = {random_offer[2][2]}, offer = {type="give-item", item="coin", count=random_offer[2][1]}}
			--game.print("Selling " .. random_offer[2][2][2] .. " " .. random_offer[2][2][1] .. " for " .. random_offer[2][1] .. " coins.")
		end

		market.add_market_item(offer)

		table.remove(global.offers, randomnum)
	end
end

Event.register(cell_processed, function(event)
	--Lock markets if the cell is being contested
	if not global.started then return end
	local x = event.cell.x
	local y = event.cell.y
	local team = global.cell_control[x][y]
	local capturing_team = global.capturing[x][y].team
	if global.bonuses[x][y] == "market" then
		local market = global.markets[x .. "," .. y]
		if market.operable and team ~= capturing_team then
			market.operable = false
		elseif not market.operable and team == capturing_team then
			market.operable = true
		end
	end
end)

Event.register(phase_change, function(event) --luacheck: ignore event
	if #global.offers == 0 then reset_offers() end
	for _,market in pairs(game.surfaces.Labyrinth.find_entities_filtered{name="market"}) do
		randomize_market(market)
	end
end)

Event.register(defines.events.on_player_died, function(event)
	if not event.cause then return end
	if event.cause.name == "player" then
		event.cause.insert({name="coin", count=1})
	end
end)
