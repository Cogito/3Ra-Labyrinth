--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 of 3RaGaming
--This file holds the functions that involve adding the bonuses to the generated Labyrinth

require "locale/utils/event"
require "markets"

cell_captured = cell_captured or script.generate_event_name()

--luacheck: ignore add_bonuses
function add_bonuses()
	local cells = {}
	
	for x=1,global.mazesize do
		for y=1,global.mazesize do
			table.insert(cells, {x=x,y=y})
		end
	end
	
	while true do
		if #cells == 0 then break end
		local current = math.random(#cells)
		local x = cells[current].x
		local y = cells[current].y
		table.remove(cells, current)
		
		local top_left = {x = global.top_left.x + ((x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((y - 1) * (global.cellsize + 1))}
		local bottom_right = {x = global.top_left.x + (x * (global.cellsize + 1)), y = global.top_left.y + (y * (global.cellsize + 1))}
		local center = {x = ((top_left.x + bottom_right.x) / 2) + 3, y = ((top_left.y + bottom_right.y) / 2)}

		if global.cellsize % 2 == 0 then
			center.x = center.x + 1
			center.y = center.y + 1
		end

		if global.cellsize < 25 and x % 2 == 1 and y % 2 == 0 then
			--Move the bonus to be in the center if there isn't a roboport
			center.x = center.x - 3
		end
		
		local accumulator_chance = 10
		local assembly_chance = accumulator_chance + 25
		local turret_chance = assembly_chance + 10
		local oil_chance = turret_chance + 10
		local market_chance = oil_chance + 5
		local mine_chance = market_chance + 40
		
		local randomnum = math.random(mine_chance)
		
		if randomnum > 0 and randomnum <= accumulator_chance then
			--Add an electric-energy-interface
			--An electric-energy-interface allows us to fine tune exactly what kind of bonus this provides.
			--It also allows it to handle refilling its own energy rather than relying on the scenario to refill it
			local bonus = game.surfaces["Labyrinth"].create_entity{name="electric-energy-interface", position=center, force="Labyrinth"}
			bonus.destructible = false
			bonus.minable = false
			bonus.operable = false

			--Set the energy bonuses
			bonus.electric_buffer_size = 10000000
			bonus.electric_input_flow_limit = 0
			bonus.electric_output_flow_limit = 36000000
			bonus.electric_drain = 0
			bonus.power_production = 36000000
			bonus.power_usage = 0

			global.bonuses[x][y] = "electric-energy-interface"
			global.interfaces[x .. "," .. y] = bonus
		elseif randomnum > accumulator_chance and randomnum <= assembly_chance then
			--Add an assembling machine
			local bonus = game.surfaces["Labyrinth"].create_entity{name="assembling-machine-3", position=center, force="Labyrinth"}
			bonus.destructible = false
			bonus.minable = false
			table.insert(global.refill_energy, {cell = {x=x,y=y}, entity = bonus})
			table.insert(global.assembling_machines, {cell = {x=x,y=y}, entity = bonus})
			center.x = center.x + 2
			local inserter = game.surfaces["Labyrinth"].create_entity{name="stack-inserter", position=center, force="Labyrinth", direction=defines.direction.west}
			inserter.destructible = false
			inserter.minable = false
			table.insert(global.refill_energy, {cell = {x=x,y=y}, entity = inserter})
			--Recipes are set below
			--uranium_ammo 5% (1)
			--grenade 5% (2)
			--low_density 5% (3)
			--rocket_fuel 5% (4)
			--rocket_control 5% (5)
			--robot_frame 10% (6-7)
			--speed_module 5% (8)
			--efficiency_module 5% (9)
			--productivity_module 5% (10)
			--red_science 5% (11)
			--green_science 5% (12)
			--blue_science 5% (13)
			--military_science 5% (14)
			--production_science 5% (15)
			--high_tech_science 5% (16)
			--solar_panel 10% (17-18)
			--accumulator 10% (19-20)
			--Total 100% (1-20)
			
			local recipe
			local randomnum2 = math.ceil(math.random(100) / 5)
			
			if randomnum2 == 1 then
				recipe = "piercing-rounds-magazine"
			elseif randomnum2 == 2 then
				recipe = "grenade"
			elseif randomnum2 == 3 then
				recipe = "low-density-structure"
			elseif randomnum2 == 4 then
				recipe = "rocket-fuel"
			elseif randomnum2 == 5 then
				recipe = "rocket-control-unit"
			elseif randomnum2 == 6 or randomnum2 == 7 then
				recipe = "flying-robot-frame"
			elseif randomnum2 == 8 then
				recipe = "speed-module-2"
			elseif randomnum2 == 9 then
				recipe = "effectivity-module-2"
			elseif randomnum2 == 10 then
				recipe = "productivity-module-2"
			elseif randomnum2 == 11 then
				recipe = "science-pack-1"
			elseif randomnum2 == 12 then
				recipe = "science-pack-2"
			elseif randomnum2 == 13 then
				recipe = "science-pack-3"
			elseif randomnum2 == 14 then
				recipe = "military-science-pack"
			elseif randomnum2 == 15 then
				recipe = "production-science-pack"
			elseif randomnum2 == 16 then
				recipe = "high-tech-science-pack"
			elseif randomnum2 == 17 or randomnum2 == 18 then
				recipe = "solar-panel"
			elseif randomnum2 == 19 or randomnum2 == 20 then
				recipe = "accumulator"
			else
				--Should not happen, but is included as fail-safe
				recipe = "lab"
			end
			
			bonus.recipe = game.forces.Labyrinth.recipes[recipe]
			global.bonuses[x][y] = recipe

			bonus.insert({name="speed-module-3", count=4})
		elseif randomnum > assembly_chance and randomnum <= turret_chance then
			--Add a turret
			
			local gun_chance = 50
			local laser_chance = gun_chance + 30
			local flame_chance = laser_chance + 20
		
			local randomnum2 = math.random(flame_chance)
			local bonus
			if randomnum2 > 0 and randomnum2 <= gun_chance then
				bonus = game.surfaces["Labyrinth"].create_entity{name="gun-turret", position=center, force="Labyrinth"}
				global.bonuses[x][y] = "gun-turret"
			elseif randomnum2 > gun_chance and randomnum2 <= laser_chance  then
				bonus = game.surfaces["Labyrinth"].create_entity{name="laser-turret", position=center, force="Labyrinth"}
				global.bonuses[x][y] = "laser-turret"
			elseif randomnum2 > laser_chance and randomnum2 <= flame_chance then
				bonus = game.surfaces["Labyrinth"].create_entity{name="flamethrower-turret", position=center, force="Labyrinth"}
				global.bonuses[x][y] = "flamethrower-turret"
			end
			bonus.destructible = false
			bonus.minable = false
		elseif randomnum > turret_chance and randomnum <= oil_chance then
			--Add an oil derrick
			local oil = game.surfaces["Labyrinth"].create_entity{name="crude-oil", position=center, force="Labyrinth"}
			local bonus = game.surfaces["Labyrinth"].create_entity{name="pumpjack", position=center, force="Labyrinth"}
			bonus.destructible = false
			bonus.minable = false
			table.insert(global.refill_energy, {cell = {x=x,y=y}, entity = bonus})
			global.bonuses[x][y] = "pumpjack"
			oil.amount = 750000
			table.insert(global.ores, oil)
			bonus.insert({name="speed-module-3", count=2})
		elseif randomnum > oil_chance and randomnum <= market_chance then
			--Add a market
			local bonus = game.surfaces["Labyrinth"].create_entity{name="market", position=center, force="Labyrinth"}
			bonus.destructible = false
			bonus.minable = false
			bonus.operable = false --Market is unusable until it is owned
			global.bonuses[x][y] = "market"
			global.markets[x .. "," .. y] = bonus
		elseif randomnum > market_chance and randomnum <= mine_chance then
			--Add a mine
			
			local uranium_chance = 10
			local iron_chance = uranium_chance + 25
			local copper_chance = iron_chance + 25
			local coal_chance = copper_chance + 25
			local stone_chance = coal_chance + 15
			
			local ore = ""
			local randomnum2 = math.random(stone_chance)
			if randomnum2 > 0 and randomnum2 <= uranium_chance then
				ore = "uranium-ore"
			elseif randomnum2 > uranium_chance and randomnum2 <= iron_chance then
				ore = "iron-ore"
			elseif randomnum2 > iron_chance and randomnum2 <= copper_chance then
				ore = "copper-ore"
			elseif randomnum2 > copper_chance and randomnum2 <= coal_chance then
				ore = "coal"
			elseif randomnum2 > coal_chance and randomnum2 <= stone_chance then
				ore = "stone"
			end
			
			local ore_ent = game.surfaces["Labyrinth"].create_entity{name=ore, position=center, force="Labyrinth"}
			ore_ent.amount = 750000
			ore_ent.minable = false --Prevents players from mining the ore. Mines can still mine when this is set
			table.insert(global.ores, ore_ent)
			
			local bonus = game.surfaces["Labyrinth"].create_entity{name="electric-mining-drill", position=center, force="Labyrinth"}
			bonus.destructible = false
			bonus.minable = false
			table.insert(global.refill_energy, {cell = {x=x,y=y}, entity = bonus})
			if ore == "uranium-ore" then table.insert(global.uranium_mines, {cell = {x=x,y=y}, entity = bonus}) end
			global.bonuses[x][y] = ore
			bonus.insert({name="speed-module-3", count=3})
		end
	end
end

--luacheck: ignore add_roboports
function add_roboports()
	for x=1,global.mazesize do
		for y=1,global.mazesize do
			local roboport
			local position = nil

			local top_left = {x = global.top_left.x + ((x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((y - 1) * (global.cellsize + 1))}
			local bottom_right = {x = global.top_left.x + (x * (global.cellsize + 1)), y = global.top_left.y + (y * (global.cellsize + 1))}
			local center = {x = ((top_left.x + bottom_right.x) / 2), y = ((top_left.y + bottom_right.y) / 2)}

			
			if global.cellsize < 25 and x % 2 == 1 and y % 2 == 1 then
				position = center
			elseif global.cellsize >= 25 then
				position = center
			end
			
			if position then
				roboport = game.surfaces["Labyrinth"].create_entity{name="roboport", position=position, force="Labyrinth"}
				roboport.minable = false
				roboport.destructible = false
				table.insert(global.refill_energy, {cell = {x=x,y=y}, entity = roboport})
				global.roboports[x .. "," .. y] = roboport
			end
		end
	end
end

Event.register(defines.events.on_tick, function(event) --luacheck: ignore event
	if not global.started then return end

	--Every tick, refill energy
	for _,bonus in pairs(global.refill_energy) do
		local team = global.cell_control[bonus.cell.x][bonus.cell.y]
		if (team == "Green" or team == "Blue" or global.activate_bonuses) and not global.deactivate_bonuses then
			bonus.entity.energy = 100000000
		end
	end
	
	--Every 10 seconds, provide assembling machines with the resources needed to craft
	--Only active if the game has been running for at least 10 minutes
	if game.tick % (60 * 10) == 0 and game.tick - global.start_tick >= (60 * 60 * 10) then
		for _,machine in pairs(global.assembling_machines) do
			local team = global.cell_control[machine.cell.x][machine.cell.y]
			if (team == "Green" or team == "Blue" or global.activate_bonuses) and not global.deactivate_bonuses then
				for _,ingredient in pairs(game.forces.Labyrinth.recipes[global.bonuses[machine.cell.x][machine.cell.y]].ingredients) do
					if machine.entity.get_inventory(defines.inventory.assembling_machine_input).get_item_count(ingredient.name) == 0 then
						machine.entity.insert({name=ingredient.name, count=ingredient.amount})
					end
				end
			end
		end
	end
	
	--Every 2 minutes, refill ore and oil
	if game.tick % (60 * 60 * 5) == 0 then
		for _,mine in pairs(global.uranium_mines) do
			local team = global.cell_control[mine.cell.x][mine.cell.y]
			if (team == "Green" or team == "Blue" or global.activate_bonuses) and not global.deactivate_bonuses then
				mine.entity.fluidbox[1] = {type="sulfuric-acid", amount=200}
			end
		end
		for _,ore in pairs(global.ores) do
			ore.amount = 750000
		end
	end
end)

--Function to break the wire connections between poles if the other pole belongs to the opposing side
local function break_connections(event)
	local ent = event.created_entity
	if not ent.valid then return end --If another event deleted this entity first
	if ent.surface.name ~= "Labyrinth" then return end
	if ent.type == "electric-pole" then
		for colour,array in pairs(ent.neighbours) do
			for _,neighbour in pairs(array) do
				if ent.force.name ~= neighbour.force.name then
					if colour == "copper" then ent.disconnect_neighbour(neighbour)
					else ent.disconnect_neighbour({wire = defines.wire_type[colour], target_entity = neighbour})
					end
				end
			end
		end
	end
end

Event.register(defines.events.on_built_entity, break_connections)
Event.register(defines.events.on_robot_built_entity, break_connections)

--Break all power connections to the old owners of a recently captured cell
Event.register(cell_captured, function(event)
	if not global.started then return end --Leave if the cell being captured caused the game to end
	local team
	if event.team == "black" then team = "Labyrinth"
	elseif event.team == "red" then return
	else team = event.team end

	if global.bonuses[event.cell.x][event.cell.y] == "electric-energy-interface" then
		global.interfaces[event.cell.x .. "," .. event.cell.y].force = team
		local top_left = {x = global.top_left.x + ((event.cell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((event.cell.y - 1) * (global.cellsize + 1))}
		local bottom_right = {x = global.top_left.x + (event.cell.x * (global.cellsize + 1)), y = global.top_left.y + (event.cell.y * (global.cellsize + 1))}
		for _,ent in pairs(game.surfaces.Labyrinth.find_entities_filtered{type = "electric-pole", area = {top_left, bottom_right}}) do
			break_connections({created_entity = ent})
		end
	end

	if global.roboports[event.cell.x .. "," .. event.cell.y] then
		global.roboports[event.cell.x .. "," .. event.cell.y].force = team
	end

	if global.radars[event.cell.x .. "," .. event.cell.y] then
		global.radars[event.cell.x .. "," .. event.cell.y].force = team
	end
end)

local wires = {
	["copper-cable"] = "copper",
	["red-wire"] = "red",
	["green-wire"] = "green"
}

Event.register(defines.events.on_player_cursor_stack_changed, function(event)
	local player = game.players[event.player_index]
	--if player.surface.name ~= "Labyrinth" then return end
	local stack = player.cursor_stack
	local ent = player.selected
	if ent and ent.type == "electric-pole" and stack and stack.valid_for_read and wires[stack.name] then
		for _,neighbour in pairs(player.selected.neighbours[wires[stack.name]]) do
			if ent.force.name ~= neighbour.force.name then
				if wires[stack.name] == "copper" then
					ent.disconnect_neighbour(neighbour)
				else
					ent.disconnect_neighbour({wire = defines.wire_type[wires[stack.name]], target_entity = neighbour})
				end
				player.insert({name=stack.name, count=1})
				player.print({"warnings.pole"})
			end
		end
	end
end)

local ranges = {
	["small-electric-pole"] = 5,
	["medium-electric-pole"] = 7,
	["big-electric-pole"] = 4,
	["substation"] = 18
}
--Do not allow stealing energy by building a pole adjacent to an energy producer
local function deny_energy_stealing(event)
	local ent = event.created_entity
	if not ent or not ent.valid then return end --If another event deleted this entity first
	if ranges[ent.name] then
		local range = ranges[ent.name] / 2
		local top_left = {x = ent.position.x - range, y = ent.position.y - range}
		local bottom_right = {x = ent.position.x + range, y = ent.position.y + range}
		for _,near in pairs(ent.surface.find_entities_filtered{area = {top_left, bottom_right}}) do
			if near.valid and near.prototype.electric_energy_source_prototype and near.force.name ~= ent.force.name and near.name ~= "laser-turret" then
				if event.player_index then
					local player = game.players[event.player_index]
					player.insert({name=ent.name, count=1})
					player.print({"warnings.grid"})
				elseif event.robot then
					event.robot.insert({name=ent.name, count=1})
				end
				ent.destroy()
				break
			end
		end
	end
end

Event.register(defines.events.on_built_entity, deny_energy_stealing)
Event.register(defines.events.on_robot_built_entity, deny_energy_stealing)
